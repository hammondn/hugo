## Front Page Content

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.


## Here is a starting point for what we need when it comes to dependencies:
 
You should have an understanding (in other words, a list) of the artifacts upon which your project or application depends. These include:

- All source code folders in TFS which are used for the build
- Any assemblies that are referenced from:
  - The Global Assembly Cache
  - A folder on a shared server (bld62hi, for example)
  - An artifact that is checked into source control (dll’s in a bin folder, for example)
- Any jar files required for build (Oracle drivers like ojdbc6.jar, for example)
- Any non-standard linux scripts used during build
 
## Here are some thoughts I have on how to communicate:
 
Static public page (maybe gitlab pages?)

- Reference info on deployable units
- Reference info about git/gitlab qualification
- Overview of TFS to Gitlab
- Rules of engagement for TFS to Gitlab help

Article in developer newsletter pointing to and/or summarizing the above
A couple of 2-hour labs per week – set time and place where people can just drop in