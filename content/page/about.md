---
title: TFS to Gitlab Hub
subtitle: (tfs,gitlab) => migrateProject.from(tfs).to(gitlab); 
comments: false
---
The objective of this document is to provide guidance and support to the development 
community as we transition our application source code and build scripts from TFS
to Gitlab.

Here are the steps you should follow:

- [Get your team qualified](#qualified)
- [Gather all your projects dependencies](#dependencies)
- [Consult with the architecture services team](#meet)
- [Educate yourself on packaging principles](#educate)
- [Create repository(ies), migrate code, and set up a CI pipeline](#create)
- [Deploy](#deploy)


### <a name="qualified"></a>Get your team qualified

In order to maintain the integrity of the repository as a whole, it is important
that every developer on your team be proficient with Git. Qual cards help to a.) 
identify key concepts and b.) evaluate proficiency of a given skill set. 

### <a name="dependencies"></a>Gather all of your project's dependencies

Once your team is fully qualified, the next step is to gather a list of all the 
dependencies that go into building your application. This includes:

- The location of all source code folders in TFS that go into your app
- Any assemblies that are referenced from:
  - The Global Assembly Cache
  - A folder on a shared server (bld62hi, for example)
  - An artifact that is checked into source control (dll’s in a bin folder, for example)
- Any jar files required for build (Oracle drivers like ojdbc6.jar, for example)
- Any non-standard linux scripts used during build

### <a name="meet"></a>Consult with the architecture services team

The architecture services team will support you throughout the process, and will also
review your repository and CI setup to make sure they are well-designed. Meet with 
[Steven Clanton](mailto:steven_clanton@ncci.com) to get started.

### <a name="educate"></a>Educate yourself on packaging principles

Setting up your repository or repositories implies determining the optimal breakdown
of "deployable units" based on good packaging principles. Here are some good resources:

Packaging principles

- [http://randycoulman.com/blog/2014/01/28/packaging-principles-part-1/]
- [http://randycoulman.com/blog/2014/02/04/packaging-principles-part-2/]
- [http://randycoulman.com/blog/2014/02/11/packaging-principles-part-3/]

Architecting for continuous delivery

- [https://www.thoughtworks.com/insights/blog/architecting-continuous-delivery]

### <a name="create"></a>Create repository(ies), migrate code, and set up a CI pipeline

Migrate your code into the repository(ies) you have designed based on the previous
steps. Using the CI/CD features of GitLab, set up your application to integrate continuously.

Here's some documentation:
[https://docs.gitlab.com/ee/ci/]

### <a name="deploy"></a>Deploy

